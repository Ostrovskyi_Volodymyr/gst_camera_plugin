## This is a port of the PX4 camera plugin for Classic Gazebo, but for Ardupilot and GAZEBOSIM 7.5.0

A Gazebo plugin that can be attached to a camera and then streams the video data using gstreamer.  
It streams to a configurable UDP IP and UDP Port, defaults are respectively 127.0.0.1 and 5600.

Connect to the stream via command line with:
```
gst-launch-1.0 -v udpsrc port=5600 caps='application/x-rtp, media=(string)video, clock-rate=(int) 90000, encoding-name=(string)H264' \
! rtph264depay ! avdec_h264 ! videoconvert ! autovideosink sync=false
```

An example of a model configuration for attaching a plugin to a camera:
```
  <link name='cam_link'>
  ...
  <sensor name='camera1' type='camera'>
    <camera name="head">
    ...
    </camera>
    <plugin name="GstCameraPlugin" filename="GstCameraPlugin">
      <udpPort>9001</udpPort>
    </plugin>
  </sensor>
  ...
  </link>
```
